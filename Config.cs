using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace xlsgrep
{
    class Config
    {
        public static readonly Config I = new Config();

        /// <summary>Application exe name (most probably "xlsgrep")</summary>
        public readonly string AppName;

        private readonly List<Regex> _filenamePatternsInclude;
        private readonly List<Regex> _filenamePatternsExclude;
        private readonly List<Regex> _directoryPatternsExclude;


        /// <summary>Filenames to search. Any filename may contain wildcards or specify a directory instead of a file.</summary>
        public string[] Filenames { get; private set; }

        /// <summary>Search only files that match one of these regex patterns.</summary>
        public Regex[] FilenamePatternsInclude { get { return _filenamePatternsInclude.ToArray(); } }

        /// <summary>Skip files and directories that match one of these regex patterns.</summary>
        public Regex[] FilenamePatternsExclude { get { return _filenamePatternsExclude.ToArray(); } }

        /// <summary>Skip directories that match one of these regex patterns.</summary>
        public Regex[] DirectoryPatternsExclude { get { return _directoryPatternsExclude.ToArray(); } }

        /// <summary>Password for Excel file.</summary>
        public string Password { get; private set; }

        /// <summary>Pattern to find. If <see cref="SearchPattern"/> is <c>true</c> this contains a regular expression.</summary>
        public string SearchPattern { get; private set; }

        /// <summary>Ignore case distinctions</summary>
        public bool IgnoreCase { get; private set; }

        /// <summary>If <c>true</c>, <see cref="SearchPattern"/> must match whole words.</summary>
        public bool MatchWord { get; private set; }

        /// <summary>If <c>true</c>, <see cref="SearchPattern"/> must match whole cells.</summary>
        public bool MatchCell { get; private set; }

        /// <summary>Specifies, whether <see cref="SearchPattern"/> contains a regular expression or not.</summary>
        public bool IsRegex { get; private set; }

        /// <summary>If <c>true</c> a recursive file search is done if <see cref="Filenames"/> contains directories or wildcards.</summary>
        public bool Recursive { get; private set; }

        /// <summary>Suppress error messages.</summary>
        public bool SuppressErrorMessages { get; private set; }

        /// <summary>Suppress output.</summary>
        public bool Quiet { get; private set; }

        /// <summary>Stop after NUM selected lines.</summary>
        public int MaxCount { get; private set; }

        /// <summary>
        /// If <c>true</c>, always print filenames with output;
        /// if <c>false</c>, never print filenames;
        /// if <c>null</c>, print filenames if more than one file might be affected.
        /// </summary>
        public bool? IncludeFilename { get; private set; }

        /// <summary>
        /// If <c>true</c>, print only names of files with matching cells;
        /// if <c>false</c>, print only names of files with non-matching cells;
        /// if <c>null</c>, print matching cells (default).
        /// </summary>
        public bool? PrintFilenamesWithMatches { get; private set; }

        /// <summary>
        /// If <c>true</c> (and <see cref="PrintFilenamesWithMatches" /> == <c>null</c>), print matching cell addresses (default);
        /// suppress otherwise.
        /// </summary>
        public bool PrintCellAddressWithMatches { get; private set; }

        /// <summary>Print only a count of selected cells per file.</summary>
        public bool PrintCount { get; private set; }

        /// <summary>Separator char after filename and cell address.</summary>
        public char SeparatorChar { get; private set; }

        /// <summary>If <c>true</c> flush every line on stdout.</summary>
        public bool FlushOutput { get; private set; }

        /// <summary>Undocumented: List all file contents prior search.</summary>
        public bool ListCells { get; private set; }

        /// <summary>Display help text and exit.</summary>
        public bool ShowHelp { get; private set; }

        /// <summary>Display version information and exit</summary>
        public bool ShowVersion { get; private set; }

        private Config() {
            AppName = System.AppDomain.CurrentDomain.FriendlyName;
            SeparatorChar = ':';
            _filenamePatternsInclude = new List<Regex> { new Regex(@"^.*\.xl(s[xmb]|am|t[xm])$", RegexOptions.IgnoreCase) };
            _filenamePatternsExclude = new List<Regex>();
            _directoryPatternsExclude = new List<Regex>();
            PrintCellAddressWithMatches = true;
            MaxCount = int.MaxValue;
        }

        public void Init(string[] args) {
            var q = new ParamQueue<string>(args);
            bool loop = true;
            while (loop && q.Count > 0) {
                var p = q.Dequeue();

                // explode "stuffed" single-char arguments such as: -abc ==> -a -b -c
                if (p.Length > 2 && p[0] == '-' && p[1] != '-') {
                    foreach (char ch in p.Substring(1))
                        q.Push("-" + ch);
                    continue;
                }

                if (p.StartsWith("--") && p.Contains('=')) {
                    var parts = p.Split('=', 2);
                    q.Push(parts[1]);
                    q.Push(parts[0]);
                    continue;
                }

                try {
                    bool done = false;
                    done = done || ReadParamPatternSelection(p, q);
                    done = done || ReadParamMisc(p, q);
                    done = done || ReadParamOutputControl(p, q);

                    if (ShowHelp || ShowVersion)
                        return;

                    if (!done) {
                        if (p.StartsWith("-"))
                            throw new ArgumentException("unknown option " + p);
                        if (SearchPattern == null)
                            SearchPattern = p;
                        else
                            q.Push(p);  // SearchPattern was filled before --> p is a file
                        loop = false;
                    }
                } catch (Exception e) {
                    throw new ArgumentException(GetErrorMessage(p, e), e);
                }
            }
            Filenames = q.ToArray();
            if (Filenames.Length == 0)
                Filenames = Recursive ? new[] { "." } : new[] { "-" };
            if (SearchPattern == null)
                throw new ArgumentException("Nothing to do.");
        }

        private bool ReadParamPatternSelection(string p, ParamQueue<string> q) {
            switch (p) {
                // Pattern selection and interpretation:
                case "-E":
                case "--extended-regexp":
                    IsRegex = true;
                    return true;

                case "-f":
                case "--file":
                    SearchPattern = File.ReadAllText(q.Dequeue()).Trim();
                    return true;

                case "-i":
                case "--ignore-case":
                    IgnoreCase = true;
                    return true;

                case "-w":
                case "--word-regexp":
                    MatchWord = true;
                    return true;

                case "-x":
                case "--line-regexp":
                    MatchCell = true;
                    return true;

                default:
                    return false;
            }
        }

        private bool ReadParamMisc(string p, ParamQueue<string> q) {
            switch (p) {
                // Miscellaneous:
                case "-s":
                case "--no-messages":
                    SuppressErrorMessages = true;
                    return true;

                case "-V":
                case "--version":
                    ShowVersion = true;
                    return true;

                case "--help":
                    ShowHelp = true;
                    return true;

                default:
                    return false;
            }
        }

        private bool ReadParamOutputControl(string p, ParamQueue<string> q) {
            switch (p) {
                // Output control:
                case "-m":
                case "--max-count":
                    MaxCount = int.Parse(q.Dequeue());
                    return true;

                case "-N":
                case "--no-line-number":
                    PrintCellAddressWithMatches = false;
                    return true;

                case "-n":
                case "--line-number":
                    PrintCellAddressWithMatches = true;
                    return true;

                case "--line-buffered":
                    FlushOutput = true;
                    return true;

                case "-H":
                case "--with-filename":
                    IncludeFilename = true;
                    return true;

                case "-h":
                case "--no-filename":
                    IncludeFilename = false;
                    return true;

                case "-q":
                case "--quiet":
                case "--silent":
                    Quiet = true;
                    return true;

                case "-r":
                case "-R":
                case "--recursive":
                    Recursive = true;
                    return true;

                case "-p":
                case "--password":
                    Password = q.Dequeue();
                    return true;

                case "--include":
                    string pattern = q.Dequeue();
                    if (string.IsNullOrEmpty(pattern))
                        _filenamePatternsInclude.Clear();
                    else
                        _filenamePatternsInclude.Add(new WildcardRegex(pattern));
                    return true;

                case "--exclude":
                    _filenamePatternsExclude.Add(new WildcardRegex(q.Dequeue()));
                    return true;

                case "--exclude-dir":
                    _directoryPatternsExclude.Add(new WildcardRegex(q.Dequeue()));
                    return true;

                case "--exclude-from":
                    var efFilename = q.Dequeue();
                    try {
                        _filenamePatternsExclude.AddRange(File.ReadAllLines(efFilename)
                            .Where(x => !string.IsNullOrWhiteSpace(x))
                            .Select(x => new WildcardRegex(x) as Regex));
                    } catch (FileNotFoundException) {
                        throw;
                    } catch (Exception e) {
                        if (Directory.Exists(efFilename))
                            new ArgumentException(efFilename + ": Is a directory", e);
                        else
                            throw;
                    }
                    return true;

                case "-L":
                case "--files-without-match":
                    PrintFilenamesWithMatches = false;
                    return true;

                case "-l":
                case "--files-with-matches":
                    PrintFilenamesWithMatches = true;
                    return true;

                case "-c":
                case "--count":
                    PrintCount = true;
                    return true;

                case "-Z":
                case "--null":
                    SeparatorChar = '\0';
                    return true;

                case "--list-cells":
                    ListCells = true;
                    return true;

                default:
                    return false;
            }
        }

        private string GetErrorMessage(string p, Exception e) {
            switch (p) {
                case "-m":
                case "--max-count":
                    if (e is FormatException)
                        return "Invalid max count";
                    break;
            }
            var fnfe = e as FileNotFoundException;
            if (fnfe != null)
                return fnfe.FileName + ": No such file or directory";
            if (e is InvalidOperationException)
                return "option requires an argument -- " + p;
            return e.Message;
        }

        public void LogError(string msg, string filename) {
            LogError(filename + ": " + msg);
        }

        public void LogError(string msg) {
            if (!SuppressErrorMessages) {
                Console.Error.WriteLine($"{Config.I.AppName}: {msg}");
                if (FlushOutput)
                    Console.Error.Flush();
            }
        }

        public enum Result
        {
            AnySelected = 0,
            NoneSelected = 1,
            Error = 2
        }

        private class ParamQueue<T> : LinkedList<T>
        {
            public ParamQueue(IEnumerable<T> args) : base(args) { }
            public T Dequeue() {
                var res = First;
                RemoveFirst();
                return res == null ? default(T) : res.Value;
            }

            public void Enqueue(T value) {
                AddLast(value);
            }

            public void Push(T value) {
                AddFirst(value);
            }
        }
    }
}