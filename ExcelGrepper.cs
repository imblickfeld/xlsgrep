using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using OfficeOpenXml;

namespace xlsgrep
{
    class ExcelGrepper
    {
        private readonly Config _cfg = Config.I;

        public Config.Result Grep() {
            var result = Config.Result.NoneSelected;
            var matches = new Dictionary<ExcelKey, string>();

            string searchPattern = _cfg.SearchPattern;
            string[] filenamePatterns = _cfg.Filenames;
            string password = _cfg.Password;
            bool quiet = _cfg.Quiet;
            int maxCount = quiet ? 1 : _cfg.MaxCount;
            bool listCells = _cfg.ListCells && !quiet;
            bool ignoreCase = _cfg.IgnoreCase;
            bool? printFilenamesWithMatches = _cfg.PrintFilenamesWithMatches;
            if (printFilenamesWithMatches != null)
                maxCount = 1;
            var comparer = _cfg.MatchCell ? new Func<string, string, StringComparison, bool>(StringEquals) : StringContains;

            bool isRegex = _cfg.IsRegex;
            if (_cfg.MatchCell) {
                if (isRegex)
                    searchPattern = "^(" + searchPattern + ")$";
            } else if (_cfg.MatchWord) {
                searchPattern = @"\b(" + (isRegex ? searchPattern : Regex.Escape(searchPattern)) + @")\b";
                isRegex = true;
            }
            var regex = isRegex ? new Regex(searchPattern, ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None) : null;

            bool includeStackTrace = true;
            IEnumerable<KeyValuePair<EnumerateFilesType, string>> filenamesAndErrors = GetFilenames(out bool includeFilename);
            if (_cfg.IncludeFilename != null)
                includeFilename = _cfg.IncludeFilename.Value;
            if (printFilenamesWithMatches != null)
                includeFilename = true;
            foreach (var item in filenamesAndErrors) {
                if (item.Key == EnumerateFilesType.ERROR) {
                    _cfg.LogError(item.Value);
                    result = Config.Result.Error;
                    continue;
                }
                string filename = item.Value;
                try {
                    includeStackTrace = false;
                    ExcelPackage p;
                    if (filename == "-")
                        p = new ExcelPackage(new StdIn(), password);
                    else {
                        FileInfo f = new FileInfo(filename);
                        if (!f.Exists)
                            throw new FileNotFoundException("No such file or directory");
                        p = new ExcelPackage(f, password);
                    }
                    using (p) {
                        includeStackTrace = true;
                        if (listCells && includeFilename)
                            Console.WriteLine("> " + filenamesAndErrors);
                        int matchCount = 0;
                        foreach (var sheet in p.Workbook.Worksheets) {
                            if (listCells)
                                Console.WriteLine("\nTabelle: " + sheet.Name);
                            var d = sheet.Dimension;
                            if (d == null)
                                continue;
                            if (listCells)
                                Console.WriteLine($"\nDimension: {d.Start.Address} ({d.Start.Column}/{d.Start.Row}) - {d.End.Address} ({d.End.Column}/{d.End.Row})");
                            for (int x = d.Start.Column; x <= d.End.Column; ++x) {
                                for (int y = d.Start.Row; y <= d.End.Row; ++y) {
                                    var cell = sheet.Cells[y, x];
                                    var o = cell.Value;
                                    if (listCells)
                                        Console.WriteLine(cell.Address + "\t" + (o ?? ""));
                                    if (o == null)
                                        continue;
                                    string s = o.ToString();
                                    if (isRegex) {
                                        if (!regex.IsMatch(s))
                                            s = null;
                                    } else if (!comparer(s, searchPattern, ignoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture))
                                        s = null;
                                    if (s != null) {
                                        if (quiet)
                                            return Config.Result.AnySelected;
                                        if (printFilenamesWithMatches != false)
                                            matches.Add(new ExcelKey(filename, cell, includeFilename), s);
                                        ++matchCount;
                                        if (result != Config.Result.Error)
                                            result = Config.Result.AnySelected;
                                        break;
                                    }
                                }
                                if (matchCount >= maxCount)
                                    break;
                            }
                            if (matchCount >= maxCount)
                                break;
                        }
                        if (printFilenamesWithMatches == false && matchCount == 0)
                            matches.Add(new ExcelKey(filename), null);
                        if (listCells)
                            ConsoleWriteLine();
                    }
                } catch (Exception e) {
                    string msg = includeStackTrace ? e.ToString() : e.Message;
                    if (includeFilename)
                        _cfg.LogError(msg, filename);
                    else
                        _cfg.LogError(msg);
                    result = Config.Result.Error;
                }
            }
            Print(matches, includeFilename);
            return result;
        }

        private IEnumerable<KeyValuePair<EnumerateFilesType, string>> GetFilenames(out bool includeFilename) {
            string[] filenames = _cfg.Filenames;
            includeFilename = filenames.Length > 1 || filenames.Any(x => Directory.Exists(x) || x.Contains('*') || x.Contains('?'));
            return EnumerateFiles().Distinct();
        }

        private IEnumerable<KeyValuePair<EnumerateFilesType, string>> EnumerateFiles() {
            bool recursive = _cfg.Recursive;
            string[] filenamePatterns = _cfg.Filenames;
            Regex[] filenamePatternsRegex = filenamePatterns.Select(x => new WildcardRegex(x)).ToArray();
            Regex[] filenamePatternsInclude = _cfg.FilenamePatternsInclude;
            Regex[] filenamePatternsExclude = _cfg.FilenamePatternsExclude;
            Regex[] directoryPatternsExclude = _cfg.DirectoryPatternsExclude;
            bool checkIncludePattern = filenamePatternsInclude.Length > 0;

            var filesToProcess = new Queue<string>(filenamePatterns);
            while (filesToProcess.Any()) {
                var path = filesToProcess.Dequeue();
                if (path == "-") { // stdin
                    yield return new KeyValuePair<EnumerateFilesType, string>(EnumerateFilesType.FILE, path);
                    continue;
                }
                if (string.IsNullOrEmpty(path) && recursive)
                    path = ".";
                if (path.Contains('*') || path.Contains('?')) {
                    string err = AddFilesToProcess(filesToProcess, path, false, recursive);
                    if (err != null)
                        yield return new KeyValuePair<EnumerateFilesType, string>(EnumerateFilesType.ERROR, err);
                    continue;
                }

                bool isPathDir = Directory.Exists(path);
                if (!(isPathDir || filenamePatternsRegex.Any(x => x.IsMatch(path))))
                    continue;
                var filename = Path.GetFileName(path);
                if (filenamePatternsExclude.Any(x => x.IsMatch(filename)))
                    continue;
                if (isPathDir) {
                    if (!recursive)
                        yield return new KeyValuePair<EnumerateFilesType, string>(EnumerateFilesType.ERROR, $"{path}: Is a directory");
                    else if (directoryPatternsExclude.Any(x => x.IsMatch(filename)))
                        continue;
                    else {
                        string err = AddFilesToProcess(filesToProcess, path, true, recursive);
                        if (err != null)
                            yield return new KeyValuePair<EnumerateFilesType, string>(EnumerateFilesType.ERROR, err);
                    }
                    continue;
                }
                if (!File.Exists(path)) {
                    yield return new KeyValuePair<EnumerateFilesType, string>(EnumerateFilesType.ERROR, $"{path}: No such file or directory");
                    continue;
                }
                if (checkIncludePattern && !filenamePatternsInclude.Any(x => x.IsMatch(filename)))
                    continue;
                if (path.StartsWith(".\\") || path.StartsWith("./"))
                    path = path.Substring(2);
                yield return new KeyValuePair<EnumerateFilesType, string>(EnumerateFilesType.FILE, path);
            }
        }

        private static readonly EnumerationOptions ENUM_OPTIONS = new EnumerationOptions {
            IgnoreInaccessible = true,
            MatchCasing = MatchCasing.PlatformDefault,
            RecurseSubdirectories = false,
            ReturnSpecialDirectories = false
        };

        private static string AddFilesToProcess(Queue<string> filesToProcess, string path, bool pathIsDirectory, bool recursive) {
            string dir = pathIsDirectory ? path : Path.GetDirectoryName(path);
            try {
                if (string.IsNullOrEmpty(dir))
                    dir = ".";
                var before = filesToProcess.Count;
                filesToProcess.EnqueueRange(Directory.EnumerateFiles(
                    dir,
                    pathIsDirectory ? "*" : Path.GetFileName(path),
                    ENUM_OPTIONS));
                if (recursive)
                    filesToProcess.EnqueueRange(Directory.EnumerateDirectories(dir, "*", ENUM_OPTIONS));
                if (filesToProcess.Count == before)
                    return $"{path}: No such file or directory";
                return null;
            } catch (DirectoryNotFoundException) {
                return $"{dir}: No such directory";
            }
        }

        private static bool IsMatchAny(string filename, IEnumerable<Regex> patterns) {
            return patterns.Any(x => x.IsMatch(filename));
        }

        private static bool StringContains(string s, string value, StringComparison comparisonType) {
            return s.Contains(value, comparisonType);
        }

        private static bool StringEquals(string s, string value, StringComparison comparisonType) {
            return s.Equals(value, comparisonType);
        }

        private void Print(Dictionary<ExcelKey, string> res, bool includeFilename) {
            bool printCount = _cfg.PrintCount;
            char separatorChar = _cfg.SeparatorChar;
            bool ZeroEnding = separatorChar == '\0';
            if (printCount || _cfg.PrintFilenamesWithMatches != null) {
                foreach (var group in res.GroupBy(x => x.Key.Filename).OrderBy(x => x.Key)) {
                    if (includeFilename) {
                        Console.Write(group.Key);
                        if (printCount || ZeroEnding)
                            Console.Write(separatorChar);
                    }
                    if (printCount)
                        Console.WriteLine(group.Count());
                    if (!ZeroEnding)
                        ConsoleWriteLine();
                }
                return;
            }
            foreach (var cell in res.OrderBy(x => x.Key)) {
                Console.Write(cell.Key);
                Console.Write(cell.Value);
                ConsoleWriteLine();
            }
        }

        private void ConsoleWriteLine() {
            Console.WriteLine();
            if (_cfg.FlushOutput)
                Console.Out.Flush();
        }

        private enum EnumerateFilesType
        {
            FILE, ERROR
        }

        private class ExcelKey : IComparable<ExcelKey>
        {
            public readonly string Filename;
            public readonly string CellAddress;
            public readonly bool IncludeFilename;

            private readonly string _worksheet;
            private readonly int _x;
            private readonly int _y;
            private readonly bool _printCellAddress;

            public ExcelKey(string filename, ExcelRange cell, bool includeFilename = false) {
                Filename = filename;
                _worksheet = cell.Worksheet.Name;
                CellAddress = _worksheet + "!" + cell.Address;
                IncludeFilename = includeFilename;
                _x = cell.Columns;
                _y = cell.Rows;
                _printCellAddress = Config.I.PrintCellAddressWithMatches;
            }

            public ExcelKey(string filename) {
                Filename = filename;
            }

            public override string ToString() {
                char separatorChar = Config.I.SeparatorChar;
                return (IncludeFilename ? Filename + separatorChar : "")
                    + (_printCellAddress ? CellAddress + separatorChar : "");
            }

            public override int GetHashCode() {
                return Filename.GetHashCode() ^ (CellAddress == null ? 0 : CellAddress.GetHashCode());
            }

            public override bool Equals(object obj) {
                var other = obj as ExcelKey;
                if (other == null)
                    return false;
                return other.Filename == Filename && other.CellAddress == CellAddress;
            }

            public int CompareTo([AllowNull] ExcelKey other) {
                if (other == null)
                    return 1;
                int c = Filename.CompareTo(other.Filename);
                if (c != 0)
                    return c;
                if (_worksheet == null)
                    return -1;
                c = _worksheet.CompareTo(other._worksheet);
                if (c != 0)
                    return c;
                c = _x.CompareTo(other._x);
                return c == 0 ? _y.CompareTo(other._y) : c;
            }
        }

        /// <summary>Memory copy of standard input stream.</summary>
        private class StdIn : MemoryStream
        {
            public StdIn() {
                const int BUF_SIZE = 16 * 1024;
                byte[] buf = new byte[BUF_SIZE];
                using (var stdin = Console.OpenStandardInput(BUF_SIZE)) {
                    int read;
                    while ((read = stdin.Read(buf, 0, BUF_SIZE)) > 0)
                        Write(buf, 0, read);
                }
            }
        }
    }
}