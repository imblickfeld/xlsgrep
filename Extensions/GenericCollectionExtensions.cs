namespace System.Collections.Generic
{
    public static class GenericCollectionExtensions
    {
        public static void EnqueueRange<T>(this Queue<T> queue, IEnumerable<T> values) {
            foreach (var item in values)
                queue.Enqueue(item);
        }
    }
}