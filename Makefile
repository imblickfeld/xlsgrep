all: release

RUNTIME=win-x64
FRAMEWORK=netcoreapp3.1

RELEASE_DIR=bin/Release
PUBLISH_DIR=$(RELEASE_DIR)/$(FRAMEWORK)/$(RUNTIME)/publish
MAN_DIR=$(PUBLISH_DIR)/man

clean:
	-rm -r $(RELEASE_DIR)

build-man:
	pandoc -s -t man README.md -o man/xlsgrep.1

restore:
	dotnet restore -r $(RUNTIME)

build: restore
	dotnet build -r $(RUNTIME)

build-publish: build
	dotnet publish -f $(FRAMEWORK) -c Release -r $(RUNTIME) --no-self-contained

release: clean build-publish build-man
	mkdir -p $(MAN_DIR)
	gzip -c man/xlsgrep.1 > $(MAN_DIR)/xlsgrep.1.gz
	rm $(PUBLISH_DIR)/xlsgrep.pdb
	cd $(PUBLISH_DIR); 7za a -tzip -r ../../../xlsgrep.zip *
	rm -r $(RELEASE_DIR)/$(FRAMEWORK)
