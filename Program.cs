﻿using System;
using System.Collections.Generic;

namespace xlsgrep
{
    class Program
    {
        static int Main(string[] args) {
            try {
                Config.I.Init(args);
            } catch (ArgumentException e) {
                ShowHelp(e);
                return 1;
            }
            if (Config.I.ShowHelp) {
                ShowHelp();
                return 0;
            }
            if (Config.I.ShowVersion) {
                ShowVersion();
                return 0;
            }
            return (int)new ExcelGrepper().Grep();
        }

        public static void ShowHelp(Exception e) {
            if (e != null)
                Console.Error.WriteLine(Config.I.AppName + ": " + e.Message);
            Console.Error.WriteLine($@"Usage: {Config.I.AppName} [OPTION]... PATTERN [FILE]...
Try '{Config.I.AppName} --help' for more information.");
        }

        public static void ShowHelp() {
            Console.WriteLine($@"Usage: {Config.I.AppName} [OPTION]... PATTERN [FILE]...
Search for PATTERN in each Excel (XLSX) file.
Example: xlsgrep ""hello world"" a*.xlsx b.xlsx

Pattern selection and interpretation:
    -E, --extended-regexp   PATTERN is an extended .NET regular expression
    -f, --file=FILE         obtain PATTERN from FILE
    -i, --ignore-case       ignore case distinctions
    -w, --word-regexp       force PATTERN to match only whole words
    -x, --line-regexp       force PATTERN to match only whole cells

Miscellaneous:
    -s, --no-messages       suppress error messages
    -V, --version           display version information and exit
        --help              display this help text and exit

Output control:
    -m, --max-count=NUM     stop after NUM selected cells
    -N, --no-line-number    suppress cell address with output lines
    -n, --line-number       print cell address with output lines
        --line-buffered     flush output on every line
    -H, --with-filename     print file name with output cells
    -h, --no-filename       suppress the file name prefix on output
    -p, --password=PASSWORD  PASSWORD to open the encrypted FILE with
    -q, --quiet, --silent   suppress all normal output
    -r, --recursive         recurse directories
        --include=FILE_PATTERN  search only files that match FILE_PATTERN
                            default is *.
        --exclude=FILE_PATTERN  skip files and directories matching FILE_PATTERN
        --exclude-from=FILE   skip files matching any file pattern from FILE
        --exclude-dir=PATTERN  directories that match PATTERN will be skipped.
    -L, --files-without-match  print only names of FILEs with no selected cells
    -l, --files-with-matches  print only names of FILEs with selected cells
    -c, --count             print only a count of selected cells per FILE
    -Z, --null              print 0 byte after FILE name
            
When FILE is '-', read standard input.  With no FILE, read '.' if
recursive, '-' otherwise.  With fewer than two FILEs, assume -h.
Exit status is 0 if any line is selected, 1 otherwise;
if any error occurs and -q is not given, the exit status is 2.");
        }

        public static void ShowVersion() {
            Console.WriteLine($@"xlsgrep v{typeof(Program).Assembly.GetName().Version}
Copyright (C) 2020 software.imBlickFeld.de
License GPLv3: GNU GPL version 3 <http://www.gnu.org/licenses/gpl-3.0>.
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.

Written by Klaus Bergmann, see <https://www.imblickfeld.de/contact>.");
        }
    }
}
