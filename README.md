% XLSGREP(1) xlsgrep 1.0 | General Commands Manual

# NAME

xlsgrep - print MS Excel cells matching a pattern

# SYNOPSIS

**xlsgrep** [*OPTIONS*] *PATTERN* [*FILE*...]

# DESCRIPTION

**xlsgrep** searches the named input MS Excel _FILEs_ 
for cells containing a match to the given _PATTERN_. By default, **xlsgrep** prints the matching cells.

# OPTIONS

## Generic Program Information

**-\-help**
    Print a usage message briefly summarizing these command-line options, then exit.

**-V**, **-\-version**
:   Print the version number of **xlsgrep** to the standard output stream. This version number should be included in all bug reports.

## Matcher Selection

**-E**, **-\-extended-regexp**
:   Interpret *PATTERN* as an extended .NET regular expression.

## Matching Control

**-f**, **-\-file**=*FILE*
:   Obtain patterns from *FILE*, one per line. The empty file contains zero patterns, and therefore matches nothing.

**-i**, **-\-ignore-case**
:   Ignore case distinctions in both the *PATTERN* and the input files.

**-w**, **-\-word-regexp**
:   Select only those cells containing matches that form whole words. The test is that the matching substring must either be at the beginning of the line, or preceded by a non-word constituent character. Similarly, it must be either at the end of the line or followed by a non-word constituent character. Word-constituent characters are letters, digits, and the underscore.

**-x**, **-\-line-regexp**
:   Select only those matches that exactly match the whole cell.

## General Output Control

**-c**, **-\-count**
:   Suppress normal output; instead print a count of matching cells for each input file.

**-L**, **-\-files-without-match**
:   Suppress normal output; instead print the name of each input file from which no output would normally have been printed. The scanning will stop on the first match.

**-l**, **-\-files-with-matches**
:   Suppress normal output; instead print the name of each input file from which output would normally have been printed. The scanning will stop on the first match.

**-m** *NUM*, **--max-count**=*NUM*
:   Stop reading a file after *NUM* matching lines. When the **-c** or **--count** option is also used, grep does not output a count greater than *NUM*.

**-q**, **-\-quiet**, **-\-silent**
:   Quiet; do not write anything to standard output. Exit immediately with zero status if any match is found, even if an error was detected. Also see the *-s* or *-\-no-messages* option.

**-s**, **-\-no-messages**
:   Suppress error messages about nonexistent or unreadable files.

## Output Line Prefix Control

**-H**, **-\-with-filename**
:   Print the file name for each match. This is the default when there is more than one file to search.

**-h**, **-\-no-filename**
:   Suppress the prefixing of file names on output. This is the default when there is only one file (or only standard input) to search.

**-N**, **-\-no-line-number**
:   Suppress the prefixing of cell addresses on output.

**-n**, **-\-line-number**
:   Prefix each line of output with the cell address within its input file. This is the default.

**-Z**, **-\-null**
:   Output a zero byte (the ASCII **NUL** character) instead of the character that normally follows a file name. For example, **xlsgrep -lZ** outputs a zero byte after each file name instead of the usual newline. This option makes the output unambiguous, even in the presence of file names containing unusual characters like newlines. This option can be used with commands like **find -print0**, **perl -0**, **sort -z**, and **xargs -0** to process arbitrary file names, even those that contain newline characters.

[## Context Line Control]::

## File and Directory Selection

**-\-exclude**=*GLOB*
:   Skip files whose base name matches *GLOB* (using wildcard matching). A file-name glob can use *, ? as wildcards, and \ to quote a wildcard or backslash character literally. **-\-exclude** may appear several times.

**-\-exclude-from**=*FILE*
:   Skip files whose base name matches any of the file-name globs read from *FILE* (using wildcard matching as described under **--exclude**). 

**-\-exclude-dir**=*DIR*
:   Exclude directories matching the pattern *DIR* from recursive searches (using wildcard matching as described under **--exclude**). **-\-exclude-dir** may appear several times.

**-\-include**=*GLOB*
:   Search only files whose base name matches *GLOB* (using wildcard matching as described under **--exclude**). Default are ***.xlsx**, ***.xlsm**, ***.xlsb**, ***.xlam**, ***.xltx**, and ***.xltm**. **-\-include** may appear several times. Use **-\-include=** to clear default includes.

**-p**, **--password**=*PASSWORD*
:   *PASSWORD* to open the encrypted files with.

**-r**, **-R**, **-\-recursive**
:   Read all files under each directory, recursively.

## Other Options

**-\-line-buffered**
:   Use line buffering on output. This can cause a performance penalty.

# REGULAR EXPRESSIONS

A regular expression is a pattern that describes a set of strings. Regular expressions are constructed analogously to arithmetic expressions, by using various operators to combine smaller expressions.

**xlsgrep** understands .NET regular expression syntax.

For further information on regular expressions in .NET see
https://docs.microsoft.com/dotnet/standard/base-types/regular-expressions.

# EXIT STATUS

Normally, the exit status is 0 if selected lines are found and 1 otherwise. But the exit status is 2 if an error occurred, unless the **-q** or **-\-quiet** or **-\-silent** option is used and a selected line is found.

# COPYRIGHT

Copyright 2020 software.imBlickFeld.de

License GPLv3: GNU GPL version 3 <http://www.gnu.org/licenses/gpl-3.0>.
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions.

Written by Klaus Bergmann, see <https://www.imblickfeld.de/contact>.

# SEE ALSO

## Regular Manual Pages

grep(1)
