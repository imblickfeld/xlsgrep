using System.Text.RegularExpressions;

namespace xlsgrep
{
    class WildcardRegex : Regex
    {
        public WildcardRegex(string pattern) : base(ToRegex(pattern), RegexOptions.IgnoreCase) { }

        private static string ToRegex(string pattern) {
            return "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$";
        }
    }
}